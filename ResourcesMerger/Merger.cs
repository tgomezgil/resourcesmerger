﻿using System.Collections.Generic;

namespace ResourcesMerger
{
    using System.Collections;
    using System.Resources;

    public static class Merger
    {
        public static void Merge(string oldFile, string newFile)
        {
            Dictionary<string, string> oldEntries = GetDictionaryFromResxFile(oldFile);
            Dictionary<string, string> newEntries = GetDictionaryFromResxFile(newFile);

            using (var resourceWriter = new ResXResourceWriter(oldFile))
            {
                foreach (KeyValuePair<string, string> entry in newEntries)
                {
                    string key = entry.Key;
                    string newValue = entry.Value;

                    bool entryExistsAndIsNotEmpty = oldEntries.TryGetValue(key, out string oldValue) && !string.IsNullOrWhiteSpace(oldValue);
                    var dataNode = new ResXDataNode(key, entryExistsAndIsNotEmpty ? oldValue : newValue);
                    resourceWriter.AddResource(dataNode);
                }

                // This is required to keep entries that were added later to the old file, and therefore are not present in the new one
                foreach (KeyValuePair<string, string> entry in oldEntries)
                {
                    string key = entry.Key;
                    string value = entry.Value;
                    bool isNewEntry = !newEntries.ContainsKey(key);

                    if (isNewEntry)
                    {
                        var dataNode = new ResXDataNode(key, value);
                        resourceWriter.AddResource(dataNode);
                    }
                }

                resourceWriter.Close();
            }
        }

        private static Dictionary<string, string> GetDictionaryFromResxFile(string resxFile)
        {
            var result = new Dictionary<string, string>();

            using (var resourceReader = new ResXResourceReader(resxFile))
            {
                foreach (DictionaryEntry dictionaryEntry in resourceReader)
                {
                    result.Add(dictionaryEntry.Key.ToString(), dictionaryEntry.Value.ToString());
                }
            }

            return result;
        }
    }
}