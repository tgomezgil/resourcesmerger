﻿using System;

namespace ResourcesMerger
{
    using System.Diagnostics;
    using System.IO;

    internal class Program
    {
        private static void Main()
        {
            Console.Write("Original folder full path: ");
            string oldFolder = Console.ReadLine();

            Console.Write("New folder full path: ");
            string newFolder = Console.ReadLine();

            Debug.Assert(oldFolder != null, nameof(oldFolder) + " != null");
            Debug.Assert(newFolder != null, nameof(newFolder) + " != null");

            foreach (string oldFile in Directory.GetFiles(oldFolder, "*.resx"))
            {
                string filename = Path.GetFileName(oldFile);
                string newFile = Path.Combine(newFolder, filename);
                Merger.Merge(oldFile, newFile);
            }
        }
    }
}