﻿namespace ResourcesMerger.Tests
{
    using System.IO;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class MergerTests
    {
        [TestMethod]
        [DeploymentItem(@"TestData\identical\old.resx")]
        [DeploymentItem(@"TestData\identical\new.resx")]
        [DeploymentItem(@"TestData\identical\expected.resx")]
        public void Merge_IdenticalFiles_NoModification()
        {
            // Arrange
            const string OLD_FILE = "old.resx";
            const string NEW_FILE = "new.resx";
            const string EXPECTED = "expected.resx";

            // Act
            Merger.Merge(OLD_FILE, NEW_FILE);

            // Assert
            string modifiedFile = File.ReadAllText(OLD_FILE);
            string expected = File.ReadAllText(EXPECTED);
            modifiedFile.Should().Be(expected);
        }

        [TestMethod]
        [DeploymentItem(@"TestData\differentValue\old.resx")]
        [DeploymentItem(@"TestData\differentValue\new.resx")]
        [DeploymentItem(@"TestData\differentValue\expected.resx")]
        public void Merge_DifferentValue_NoModification()
        {
            // Arrange
            const string OLD_FILE = "old.resx";
            const string NEW_FILE = "new.resx";
            const string EXPECTED = "expected.resx";

            // Act
            Merger.Merge(OLD_FILE, NEW_FILE);

            // Assert
            string modifiedFile = File.ReadAllText(OLD_FILE);
            string expected = File.ReadAllText(EXPECTED);
            modifiedFile.Should().Be(expected);
        }

        [TestMethod]
        [DeploymentItem(@"TestData\emtpyValue\old.resx")]
        [DeploymentItem(@"TestData\emtpyValue\new.resx")]
        [DeploymentItem(@"TestData\emtpyValue\expected.resx")]
        public void Merge_EmptyValue_UpdatedWithNewValue()
        {
            // Arrange
            const string OLD_FILE = "old.resx";
            const string NEW_FILE = "new.resx";
            const string EXPECTED = "expected.resx";

            // Act
            Merger.Merge(OLD_FILE, NEW_FILE);

            // Assert
            string modifiedFile = File.ReadAllText(OLD_FILE);
            string expected = File.ReadAllText(EXPECTED);
            modifiedFile.Should().Be(expected);
        }

        [TestMethod]
        [DeploymentItem(@"TestData\missingKey\old.resx")]
        [DeploymentItem(@"TestData\missingKey\new.resx")]
        [DeploymentItem(@"TestData\missingKey\expected.resx")]
        public void Merge_MissingKey_AddedWithNewValue()
        {
            // Arrange
            const string OLD_FILE = "old.resx";
            const string NEW_FILE = "new.resx";
            const string EXPECTED = "expected.resx";

            // Act
            Merger.Merge(OLD_FILE, NEW_FILE);

            // Assert
            string modifiedFile = File.ReadAllText(OLD_FILE);
            string expected = File.ReadAllText(EXPECTED);
            modifiedFile.Should().Be(expected);
        }

        [TestMethod]
        [DeploymentItem(@"TestData\newEntry\old.resx")]
        [DeploymentItem(@"TestData\newEntry\new.resx")]
        [DeploymentItem(@"TestData\newEntry\expected.resx")]
        public void Merge_NewEntry_NoModification()
        {
            // Arrange
            const string OLD_FILE = "old.resx";
            const string NEW_FILE = "new.resx";
            const string EXPECTED = "expected.resx";

            // Act
            Merger.Merge(OLD_FILE, NEW_FILE);

            // Assert
            string modifiedFile = File.ReadAllText(OLD_FILE);
            string expected = File.ReadAllText(EXPECTED);
            modifiedFile.Should().Be(expected);
        }
    }
}